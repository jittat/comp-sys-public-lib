from course_init import *

import unittest
from unittest import main as unittest_main

def run_test(test_class):
    unittest_main(argv=[''], defaultTest=test_class.__name__, exit=False)

from basic_gates_ch02 import *
from basic_gates_ch03 import *

from compbuilder.n2t.asm import assemble
from compbuilder.n2t.sim import simulate

def listing(asm, file=None):
    out = []
    pc = 0
    for line in asm.splitlines():
        line = line.strip()
        if not line:
            continue
        if line[0] == '(':
            out.append('      ' + line)
        elif line[0] == '/':
            out.append(line)
        else:
            out.append(f'{pc:4}: {line}')
            pc += 1
    if file is not None:
        with open(file, 'w') as f:
            print('\n'.join(out), file=f)
    else:
        print('\n'.join(out))


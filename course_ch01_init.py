from course_init import *

import unittest
from unittest import main as unittest_main
from compbuilder.visual_layouts import *

def run_test(test_class):
    unittest_main(argv=[''], defaultTest=test_class.__name__, exit=False)

class Nand(NandLayoutMixin, Component):
    IN = [w.a, w.b]
    OUT = [w.out]

    PARTS = []

    def process(self, a, b):
        if (a.get()==1) and (b.get()==1):
            return {'out': Signal(0)}
        else:
            return {'out': Signal(1)}
    process_interact = process
    process_interact.js = {
        'out' : 'function(w) { return (w.a==1) && (w.b==1) ? 0 : 1; }',
    }

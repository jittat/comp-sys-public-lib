from course_init import *

from basic_gates_ch02 import *
from basic_gates_ch03 import *

def gen_fast_rom_component(address_size,name,data=[]):

    import json

    class FastROM(Component):
        IN = [w(address_size).address]
        OUT = [w(16).out]

        PARTS = []
        DATA = data
        LAYOUT_CONFIG = { 'widget': 'rom' }

        def __init__(self,**kwargs):
            super().__init__(**kwargs)

        __init__.js = '''
            function(s) {{
              s.data = {};
            }}
        '''.format(json.dumps(DATA))

        def process(self,address):
            a = address.get()
            if a < len(data) and a < 2**address_size:
                return {'out': Signal(self.DATA[a],16)}
            else:
                return {'out': Signal(0,16)}

        process_interact = process
        process_interact.js = {
            'out' : '''
                function(w,s) { // wires,states
                  return s.data[w.address];
                }''',
        }

    FastROM.__name__ = name
    return FastROM

gen_rom_component = gen_fast_rom_component

def gen_fast_ram_component(address_size,name=None):

    class FastRAMOutput(Component):
        IN = [w(address_size).address, w.load, w.latch_link]
        OUT = [w(16).out]

        PARTS = []

        def shallow_clone(self):
            return type(self)(self.buffer, **self.wire_assignments)

        def __init__(self, buffer, **kwargs):
            super(FastRAMOutput, self).__init__(**kwargs)
            self.buffer = buffer

        def process(self, address, load, latch_link):
            return {'out': Signal(self.buffer[address.get()], 16)}


    class FastRAMLatch(Component):
        IN = [w(16).In, w(address_size).address, w.load]
        OUT = [w.latch_link]

        PARTS = []

        def shallow_clone(self):
            return type(self)(self.buffer, **self.wire_assignments)

        def __init__(self, buffer, **kwargs):
            super(FastRAMLatch, self).__init__(**kwargs)
            self.buffer = buffer
            self.is_clocked_component = True
            self.saved_input_kwargs = None

        def process(self):
            if self.saved_input_kwargs:
                if self.saved_input_kwargs['load'].get() == 1:
                    address = self.saved_input_kwargs['address']
                    In = self.saved_input_kwargs['In']
                    self.buffer[address.get()] = In.get()

            return {'latch_link': Signal(0)}

        def prepare_process(self, **kwargs):
            self.saved_input_kwargs = kwargs

    class FastRAM(Component):
        IN = [w(16).In, w(address_size).address, w.load]
        OUT = [w(16).out]

        TRIGGER = [w(address_size).address, w.clk]
        LATCH = [(w(16).out, w.clk)]

        PARTS = None

        def __init__(self, **kwargs):
            super(FastRAM, self).__init__(**kwargs)
            self.buffer = [0] * (2 ** address_size)
            self.is_fast_ram = True

            self.PARTS = [
                FastRAMOutput(self.buffer,
                               In=w.In, address=w.address, load=w.load,
                               out=w.out,
                               latch_link=w.dummy),
                FastRAMLatch(self.buffer, In=w.In, address=w.address, load=w.load,
                              latch_link=w.dummy),
            ]

            # for interactive counterpart
            self._clk = Signal(0)
            self._contents = {}

        def process_interact(self,In,address,load,clk):
            if self._clk.get() == 0 and clk.get() == 1 and load.get() == 1:
                self._contents[address.get()] = In
            self._clk = clk
            return {'out': self._contents.get(address.get(),Signal(0,16))}

        process_interact.js = {
            'out' : '''
                function(w,s) { // wires,states
                  s._contents = s._contents || {};
                  if (s.clk == 0 && w.clk == 1 && w.load == 1) {
                    s._contents[w.address] = w.In;
                  }
                  s.clk = w.clk;
                  return s._contents[w.address] || 0;
                }''',
        }

    if name:
        FastRAM.__name__ = name
    else:
        size = 2**address_size
        if size >= 2**20:
            FastRAM.__name__ = f'FastRAM{size//2**20}M'
        elif size >= 2**10:
            FastRAM.__name__ = f'FastRAM{size//2**10}K'
        else:
            FastRAM.__name__ = f'FastRAM{size}'
    return FastRAM

RAM8K = gen_fast_ram_component(13,name='RAM8K')
RAM16K = gen_fast_ram_component(14,name='RAM16K')
RAM32K = gen_fast_ram_component(15,name='RAM32K')

class Screen(RAM8K):
    LAYOUT_CONFIG = { 'widget': 'screen' }


class ROM32K(Component):
    IN = [w(15).address]
    OUT = [w(16).out]

    PARTS = [
        RAM32K(In=w(16).zero,
               address=w.address,
               load=w.zero,
               out=w.out),
    ]

    def set_memory(self, idx, val):
        if not getattr(self, 'internal_components', None):
            self.initialize()

        ram = self.internal_components[0]
        ram.buffer[idx] = val


class Keyboard(Component):
    IN = []
    OUT = [w(16).out]

    PARTS = []
    LATCH = [(w(16).out, None)]
    LAYOUT_CONFIG = { 'widget': 'keyboard' }

    def __init__(self, **kwargs):
        super(Keyboard, self).__init__(**kwargs)
        self.current_key = 0

    def process(self, **kwargs):
        return {'out': Signal(self.current_key)}

    def process_interact(self,**inputs):
        return {'out': Signal(0,16)}
    process_interact.js = {
        'out': None
    }

class DFF(Component):
    IN = [w.In]
    OUT = [w.out]

    PARTS = []
    TRIGGER = [w.clk]
    LATCH = [(w.out, w.clk)]

    def __init__(self, **kwargs):
        super(DFF, self).__init__(**kwargs)
        self._clk = Signal(0) 
        self._out = Signal(0)
        self.is_clocked_component = True
        self.saved_input_kwargs = None
    __init__.js = '''
        function(s) {
          s.clk = 0;
        }
    '''
    
    def process(self, In=None, clk=None):
        if self.saved_input_kwargs == None:
            self.saved_output = {'out': Signal(0)}
        else:
            self.saved_output = {'out': self.saved_input_kwargs['In']}
        return self.saved_output

    def prepare_process(self, In):
        self.saved_input_kwargs = {'In': In}
    
    def process_interact(self,In,clk):
        if self._clk.get() == 0 and clk.get() == 1:
            self._out = In
        self._clk = clk
        return {'out':self._out}

    process_interact.js = {
        'out' : '''
            function(w,s) { // wires,states
              if (s.clk == 0 && w.clk == 1)
                s.out = w.In;
              s.clk = w.clk;
              return s.out;
            }''',
    }

class Bit(Component):
    IN = [w.In, w.load]
    OUT = [w.out]

    PARTS = [
        Mux(a=w.out, b=w.In, sel=w.load, out=w.data),
        DFF(In=w.data, out=w.out)
    ]
    
class Register(Component):
    IN = [w(16).In, w.load]
    OUT = [w(16).out]

    PARTS = []
    for i in range(16):
        PARTS.append(
            Bit(In=w.In[i], load=w.load, out=w.out[i])
        )

class Bus3(Component):
    IN = [w.In0, w.In1, w.In2]
    OUT = [w(3).out]

    PARTS = [
        Buffer(In=w.In0, out=w.out[0]),
        Buffer(In=w.In1, out=w.out[1]),
        Buffer(In=w.In2, out=w.out[2]),
    ]

class Or4Way(Component):
    IN = [w.a, w.b, w.c, w.d]
    OUT = [w.out]

    PARTS = [
        Or(a=w.a, b=w.b, out=w.aORb),
        Or(a=w.c, b=w.d, out=w.cORd),
        Or(a=w.aORb, b=w.cORd, out=w.out),
    ]

class PC(Component):
    IN = [w(16).In, w.load, w.inc, w.reset]
    OUT = [w(16).out]

    PARTS = [
        Bus3(In0=w.inc, In1=w.load, In2=w.reset, out=w(3).muxsel),
        Or4Way(a=w.reset, b=w.load, c=w.inc, d=w.F, out=w.ldreg),
        Mux8Way16(
            a=w(16).zero, 
            b=w.inc_out, 
            c=w.In, 
            d=w.In, 
            e=w(16).zero, 
            f=w(16).zero, 
            g=w(16).zero, 
            h=w(16).zero, 
            sel=w.muxsel,
            out=w(16).in_reg
        ),
        Register(In=w.in_reg, load=w.ldreg, out=w.out),
        Inc16(In=w.out, out=w(16).inc_out),
    ]

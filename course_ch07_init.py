from course_init import *
from course_ch04_init import listing

import unittest
from unittest import main as unittest_main

def run_test(test_class):
    unittest_main(argv=[''], defaultTest=test_class.__name__, exit=False)

from compbuilder.n2t.asm import assemble
from compbuilder.n2t.sim import simulate

from compbuilder.n2t.cpu_sim import PureHackCPU

from graphviz import Graph
from lark.lexer import Token
import html

NONTERM_ATTRS = {
    'shape':'plain',
    'width':'.2',
    'height':'.2',
}

TERM_ATTRS = {
    'shape':'box',
    'width':'.2',
    'height':'.2',
}

def build_graph(g,tree,start_id):
    if isinstance(tree,Token):
        g.node(f'n{start_id}',
            label=f'<{tree.type}: <FONT COLOR="brown" FACE="monospace">{html.escape(tree.value)}</FONT>>',
            **TERM_ATTRS)
        return 1
    else:
        g.node(f'n{start_id}',label=tree.data,**NONTERM_ATTRS)
        root_id = start_id
        start_id += 1
        for c in tree.children:
            child_id = start_id
            start_id += build_graph(g,c,start_id)
            g.edge(f'n{root_id}',f'n{child_id}')
        return start_id

def draw(tree):
    from IPython.display import display
    g = Graph()
    build_graph(g,tree,0)
    return display(g)

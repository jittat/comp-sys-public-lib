from course_init import *

from compbuilder.visual_layouts import *
from basic_gates_ch02 import *

class HalfAdder(Component):
    IN = [w.a, w.b]
    OUT = [w.sum, w.carry]

    PARTS = [
        Xor(a=w.a, b=w.b, out=w.sum),
        And(a=w.a, b=w.b, out=w.carry),
    ]

class FullAdder(Component):
    IN = [w.a, w.b, w.c]
    OUT = [w.sum, w.carry]

    PARTS = [
        HalfAdder(a=w.a,
                  b=w.b,
                  sum=w.s1,
                  carry=w.c1),
        HalfAdder(a=w.c,
                  b=w.s1,
                  sum=w.sum,
                  carry=w.c2),
        Or(a=w.c1,
           b=w.c2,
           out=w.carry),
    ]

class Add16(Component):
    IN = [w(16).a, w(16).b]
    OUT= [w(16).out]

    PARTS = [
    ]

    def init_parts(self):
        if Add16.PARTS:
            return

        Add16.PARTS = [
            FullAdder(a=w.a[0], b=w.b[0], c=w.zero, sum=w.out[0], carry=getattr(w, 'c0')),
        ]

        for i in range(1,16):
            Add16.PARTS.append(FullAdder(a=w.a[i],
                                         b=w.b[i],
                                         c=getattr(w, f'c{i-1}'),
                                         sum=w.out[i],
                                         carry=getattr(w, f'c{i}')))

class Inc16(Component):
    IN = [w(16).In]
    OUT= [w(16).out]

    PARTS = [
        Add16(a=w.In, b=w(16).int_one, out=w.out)    
    ]

class Buffer(BufferLayoutMixin, Component):
    IN = [w.In]
    OUT = [w.out]
    PARTS = []

    def process(self, In):
        return {'out': Signal(In.get())}
    process_interact = process
    process_interact.js = {
        'out' : 'function(w) { return w.In; }',
    }

class ALU(Component):
    IN = [w(16).x, w(16).y,
          w.zx, w.nx,
          w.zy, w.ny,
          w.f,
          w.no]

    OUT = [w(16).out, w.zr, w.ng]

    PARTS = [
        Mux16(a=w.x, b=w(16).zero, sel=w.zx, out=w(16).x1),
        Not16(In=w.x1, out=w(16).x1neg),
        Mux16(a=w.x1, b=w.x1neg, sel=w.nx, out=w(16).xfinal),
        Mux16(a=w.y, b=w(16).zero, sel=w.zy, out=w(16).y1),
        Not16(In=w.y1, out=w(16).y1neg),
        Mux16(a=w.y1, b=w.y1neg, sel=w.ny, out=w(16).yfinal),
        Add16(a=w.xfinal, b=w.yfinal, out=w(16).addresult),
        And16(a=w.xfinal, b=w.yfinal, out=w(16).andresult),
        Mux16(a=w.andresult, b=w.addresult, sel=w.f, out=w(16).funresult),
        Not16(In=w.funresult, out=w(16).notfunresult),
        Mux16(a=w.funresult, b=w.notfunresult, sel=w.no, out=w.out),

        Buffer(In=w.out[15], out=w.ng),

        Or8Way(In=w.out[0:8], out=w.bottom_some),
        Or8Way(In=w.out[8:16], out=w.top_some),
        Or(a=w.bottom_some, b=w.top_some, out=w.non_zero),
        Not(In=w.non_zero, out=w.zr),
    ]

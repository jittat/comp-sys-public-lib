import os
import gzip
import base64

vars = dict(locals(), **globals())
with gzip.open(__file__[:-3] + ".bin") as f:
    exec(base64.decodebytes(f.read()).decode(), vars, vars)

Nand = vars['Nand']
Buffer = vars['Buffer']
Not = vars['Not']
And = vars['And']
Or = vars['Or']
Xor = vars['Xor']
Mux = vars['Mux']
DMux = vars['DMux']
Not16 = vars['Not16']
And16 = vars['And16']
Or16 = vars['Or16']
Mux16 = vars['Mux16']
Or8Way = vars['Or8Way']
Mux4Way16 = vars['Mux4Way16']
Mux8Way16 = vars['Mux8Way16']
DMux4Way = vars['DMux4Way']
DMux8Way = vars['DMux8Way']

from setuptools import setup

setup(
    name='comsyslab',
    version='1.0.0',
    author='Jittat Fakcharoenphol and Chaiporn Jaikaeo',
    description='Library for computer system laboratory',
    long_description='This package contains modules to be used in assignments of computer system laboratory class at CPE, KU',
    url='',
    keywords='computer system, hdl',
    python_requires='>=3.7, <4',
)

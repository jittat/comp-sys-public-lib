import os
import gzip
import base64

vars = dict(locals(), **globals())
with gzip.open(__file__[:-3] + ".bin") as f:
    exec(base64.decodebytes(f.read()).decode(), vars, vars)

HalfAdder = vars['HalfAdder']
FullAdder = vars['FullAdder']
Add16 = vars['Add16']
Inc16 = vars['Inc16']
Buffer = vars['Buffer']
ALU = vars['ALU']

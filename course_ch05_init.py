from course_init import *

import unittest
from unittest import main as unittest_main

def run_test(test_class):
    unittest_main(argv=[''], defaultTest=test_class.__name__, exit=False)

from basic_gates_ch02 import *
from basic_gates_ch03 import *
from basic_gates_ch05 import *

from compbuilder.n2t.asm import assemble
from compbuilder.n2t.sim import simulate

from compbuilder.n2t.cpu_sim import PureHackCPU

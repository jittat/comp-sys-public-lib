from course_init import *

import unittest
from unittest import main as unittest_main

def run_test(test_class):
    unittest_main(argv=[''], defaultTest=test_class.__name__, exit=False)

from compbuilder.n2t.asm import assemble
from compbuilder.n2t.sim import simulate
from compbuilder.n2t.compiler import Joy30, TestJoy30
from compbuilder.n2t.cpu_sim import PureHackCPU

#def compile(code):
#    return Joy30().compile(Joy30.RUNTIME + code)

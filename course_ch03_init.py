from course_init import *

import unittest
from unittest import main as unittest_main

def run_test(test_class):
    unittest_main(argv=[''], defaultTest=test_class.__name__, exit=False)

from basic_gates_ch02 import *
from basic_gates_ch03 import *

def gen_fast_ram_component(address_size,name=None):

    class FastRAMOutput(Component):
        IN = [w(address_size).address, w.load, w.latch_link]
        OUT = [w(16).out]

        PARTS = []

        def shallow_clone(self):
            return type(self)(self.buffer, **self.wire_assignments)

        def __init__(self, buffer, **kwargs):
            super(FastRAMOutput, self).__init__(**kwargs)
            self.buffer = buffer

        def process(self, address, load, latch_link):
            return {'out': Signal(self.buffer[address.get()], 16)}


    class FastRAMLatch(Component):
        IN = [w(16).In, w(address_size).address, w.load]
        OUT = [w.latch_link]

        PARTS = []

        def shallow_clone(self):
            return type(self)(self.buffer, **self.wire_assignments)

        def __init__(self, buffer, **kwargs):
            super(FastRAMLatch, self).__init__(**kwargs)
            self.buffer = buffer
            self.is_clocked_component = True
            self.saved_input_kwargs = None

        def process(self):
            if self.saved_input_kwargs:
                if self.saved_input_kwargs['load'].get() == 1:
                    address = self.saved_input_kwargs['address']
                    In = self.saved_input_kwargs['In']
                    self.buffer[address.get()] = In.get()

            return {'latch_link': Signal(0)}

        def prepare_process(self, **kwargs):
            self.saved_input_kwargs = kwargs

    class FastRAM(Component):
        IN = [w(16).In, w(address_size).address, w.load]
        OUT = [w(16).out]

        TRIGGER = [w(address_size).address, w.clk]
        LATCH = [(w(16).out, w.clk)]

        PARTS = None

        def __init__(self, **kwargs):
            super(FastRAM, self).__init__(**kwargs)
            self.buffer = [0] * (2 ** address_size)
            self.is_fast_ram = True

            self.PARTS = [
                FastRAMOutput(self.buffer,
                               In=w.In, address=w.address, load=w.load,
                               out=w.out,
                               latch_link=w.dummy),
                FastRAMLatch(self.buffer, In=w.In, address=w.address, load=w.load,
                              latch_link=w.dummy),
            ]

            # for interactive counterpart
            self._clk = Signal(0) 
            self._contents = {}

        def process_interact(self,In,address,load,clk):
            if self._clk.get() == 0 and clk.get() == 1 and load.get() == 1:
                self._contents[address.get()] = In
            self._clk = clk
            return {'out': self._contents.get(address.get(),Signal(0,16))}

        process_interact.js = {
            'out' : '''
                function(w,s) { // wires,states
                  s._contents = s._contents || {};
                  if (s.clk == 0 && w.clk == 1 && w.load == 1) {
                    s._contents[w.address] = w.In;
                  }
                  s.clk = w.clk;
                  return s._contents[w.address] || 0;
                }''',
        }

    if name:
        FastRAM.__name__ = name
    else:
        size = 2**address_size
        if size >= 2**20:
            FastRAM.__name__ = f'FastRAM{size//2**20}M'
        elif size >= 2**10:
            FastRAM.__name__ = f'FastRAM{size//2**10}K'
        else:
            FastRAM.__name__ = f'FastRAM{size}'
    return FastRAM

class DFF(Component):
    IN = [w.In]
    OUT = [w.out]

    PARTS = []
    TRIGGER = [w.clk]
    LATCH = [(w.out, w.clk)]

    def __init__(self, **kwargs):
        super(DFF, self).__init__(**kwargs)
        self._clk = Signal(0)
        self._out = Signal(0)
        self.is_clocked_component = True
        self.saved_input_kwargs = None
    __init__.js = '''
        function(s) {
          s.clk = 0;
        }
    '''
    
    def process(self, In=None, clk=None):
        if self.saved_input_kwargs == None:
            self.saved_output = {'out': Signal(0)}
        else:
            self.saved_output = {'out': self.saved_input_kwargs['In']}
        return self.saved_output

    def prepare_process(self, In):
        self.saved_input_kwargs = {'In': In}
    
    def process_interact(self,In,clk):
        if self._clk.get() == 0 and clk.get() == 1:
            self._out = In
        self._clk = clk
        return {'out':self._out}

    process_interact.js = {
        'out' : '''
            function(w,s) { // wires,states
              if (s.clk == 0 && w.clk == 1)
                s.out = w.In;
              s.clk = w.clk;
              return s.out;
            }''',
    }

from course_init import *

from compbuilder.visual_layouts import *

class Nand(NandLayoutMixin, Component):
    IN = [w.a, w.b]
    OUT = [w.out]

    PARTS = []

    def process(self, a, b):
        if (a.get()==1) and (b.get()==1):
            return {'out': Signal(0)}
        else:
            return {'out': Signal(1)}
    process_interact = process
    process_interact.js = {
        'out' : 'function(w) { return (w.a==1) && (w.b==1) ? 0 : 1; }',
    }

class Buffer(BufferLayoutMixin, Component):
    IN = [w.In]
    OUT = [w.out]
    PARTS = []

    def process(self, In):
        return {'out': Signal(In.get())}
    process_interact = process
    process_interact.js = {
        'out' : 'function(w) { return w.In; }',
    }

class Not(NotLayoutMixin, Component):
    IN = [w.In]
    OUT = [w.out]

    PARTS = [
        Nand(a=w.In, b=w.In, out=w.out)
    ]


class And(AndLayoutMixin, Component):
    IN = [w.a, w.b]
    OUT = [w.out]

    PARTS = [
        Nand(a=w.a, b=w.b, out=w.c),
        Not(In=w.c, out=w.out),
    ]

class Or(OrLayoutMixin, Component):
    IN = [w.a, w.b]
    OUT = [w.out]

    PARTS = [
        Not(In=w.a, out=w.na),
        Not(In=w.b, out=w.nb),
        Nand(a=w.na, b=w.nb, out=w.out),
    ]

class Xor(XorLayoutMixin, Component):
    IN = [w.a, w.b]
    OUT = [w.out]

    PARTS = [
        Not(In=w.a, out=w.na),
        Not(In=w.b, out=w.nb),
        And(a=w.a, b=w.nb, out=w.and1),
        And(a=w.b, b=w.na, out=w.and2),
        Or(a=w.and1, b=w.and2, out=w.out),
    ]

class Mux(Mux2WayLayoutMixin, Component):
    IN = [w.a, w.b, w.sel]
    OUT = [w.out]

    PARTS = [
        Not(In=w.sel, out=w.not_sel),
        And(a=w.a, b=w.not_sel, out=w.a_sel_out),
        And(a=w.b, b=w.sel, out=w.b_sel_out),
        Or(a=w.a_sel_out, b=w.b_sel_out, out=w.out),
    ]

class DMux(DMux2WayLayoutMixin, Component):
    IN = [w.In, w.sel]
    OUT = [w.a, w.b]

    PARTS = [
        Not(In=w.sel, out=w.not_sel),
        And(a=w.In, b=w.not_sel, out=w.a),
        And(a=w.In, b=w.sel, out=w.b),
    ]


class Not16(Component):
    IN = [w(16).In]
    OUT = [w(16).out]

    PARTS = None
    
    def init_parts(self):
        if Not16.PARTS:
            return

        Not16.PARTS = []
        for i in range(16):
            Not16.PARTS.append(Not(In=w.In[i], out=w.out[i]))

class And16(Component):
    IN = [w(16).a, w(16).b]
    OUT = [w(16).out]
    PARTS = None
    
    def init_parts(self):
        if And16.PARTS:
            return

        And16.PARTS = []
        for i in range(16):
            And16.PARTS.append(And(a=w.a[i], b=w.b[i], out=w.out[i]))

class Or16(Component):
    IN = [w(16).a, w(16).b]
    OUT = [w(16).out]

    PARTS = None
    
    def init_parts(self):
        if Or16.PARTS:
            return

        Or16.PARTS = []
        for i in range(16):
            Or16.PARTS.append(Or(a=w.a[i], b=w.b[i], out=w.out[i]))


class Mux16(Mux2WayLayoutMixin, Component):
    IN = [w(16).a, w(16).b, w.sel]
    OUT = [w(16).out]

    PARTS = None
    
    def init_parts(self):
        if Mux16.PARTS:
            return

        Mux16.PARTS = []
        for i in range(16):
            Mux16.PARTS.append(Mux(a=w.a[i], b=w.b[i], sel=w.sel, out=w.out[i]))


class Or8Way(Component):
    IN = [w(8).In]
    OUT = [w.out]

    PARTS = [
        Or(a=w.In[0], b=w.In[1], out=w.o11),
        Or(a=w.In[2], b=w.In[3], out=w.o12),
        Or(a=w.In[4], b=w.In[5], out=w.o13),
        Or(a=w.In[6], b=w.In[7], out=w.o14),
        Or(a=w.o11, b=w.o12, out=w.o21),
        Or(a=w.o13, b=w.o14, out=w.o22),
        Or(a=w.o21, b=w.o22, out=w.out),
    ]

class Mux4Way16(Mux4WayLayoutMixin, Component):
    IN = [w(16).a, w(16).b, w(16).c, w(16).d, w(2).sel]
    OUT = [w(16).out]

    PARTS = [
        Mux16(a=w.a, b=w.b, sel=w.sel[0], out=w(16).out0),
        Mux16(a=w.c, b=w.d, sel=w.sel[0], out=w(16).out1),
        Mux16(a=w.out0, b=w.out1, sel=w.sel[1], out=w.out),
    ]

class Mux8Way16(Mux8WayLayoutMixin, Component):
    IN = [
        w(16).a,
        w(16).b,
        w(16).c,
        w(16).d,
        w(16).e,
        w(16).f,
        w(16).g,
        w(16).h,
        w(3).sel,
    ]
    OUT = [w(16).out]

    PARTS = [
        Mux4Way16(a=w.a, b=w.b, c=w.c, d=w.d, sel=w.sel[0:2], out=w(16).out0),
        Mux4Way16(a=w.e, b=w.f, c=w.g, d=w.h, sel=w.sel[0:2], out=w(16).out1),
        Mux16(a=w.out0, b=w.out1, sel=w.sel[2], out=w.out),
    ]

class DMux4Way(DMux4WayLayoutMixin, Component):
    IN = [w.In, w(2).sel]
    OUT = [w.a, w.b, w.c, w.d]

    PARTS = [
        DMux(In=w.In, sel=w.sel[1], a=w.otop, b=w.obottom),
        DMux(In=w.otop, sel=w.sel[0], a=w.a, b=w.b),
        DMux(In=w.obottom, sel=w.sel[0], a=w.c, b=w.d),
    ]


class DMux8Way(DMux8WayLayoutMixin, Component):
    IN = [w.In, w(3).sel]
    OUT = [w.a, w.b, w.c, w.d, w.e, w.f, w.g, w.h]

    PARTS = [
        DMux(In=w.In, sel=w.sel[2], a=w.otop, b=w.obottom),
        DMux4Way(In=w.otop, sel=w.sel[0:2], a=w.a, b=w.b, c=w.c, d=w.d),
        DMux4Way(In=w.obottom, sel=w.sel[0:2], a=w.e, b=w.f, c=w.g, d=w.h),
    ]


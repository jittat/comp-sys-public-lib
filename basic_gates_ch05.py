import os
import gzip
import base64

vars = dict(locals(), **globals())
with gzip.open(__file__[:-3] + ".bin") as f:
    exec(base64.decodebytes(f.read()).decode(), vars, vars)

gen_fast_rom_component = vars['gen_fast_rom_component']
gen_rom_component = vars['gen_rom_component']
gen_fast_ram_component = vars['gen_fast_ram_component']
Screen = vars['Screen']
ROM32K = vars['ROM32K']
RAM8K = vars['RAM8K']
RAM16K = vars['RAM16K']
RAM32K = vars['RAM32K']
Keyboard = vars['Keyboard']
DFF = vars['DFF']
Bit = vars['Bit']
Register = vars['Register']
Bus3 = vars['Bus3']
Or4Way = vars['Or4Way']
PC = vars['PC']

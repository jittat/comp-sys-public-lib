import sys
import os
import os.path

from compbuilder import Signal
from compbuilder import w
T = Signal.T
F = Signal.F

from compbuilder.tracing import report_parts, trace
from compbuilder.tracing import plot_trace, plot_trace_inout
from compbuilder.tracing import trace_and_plot_inout, trace_and_plot

from compbuilder.visual import VisualMixin
from compbuilder.visual import interact

from compbuilder import Component as GenComponent

class Component(VisualMixin, GenComponent):
    pass
